defmodule Sms506Test do
  use ExUnit.Case

  test "connect to service" do
    assert {:error, "99"} == Sms506.send_sms("81111111","prueba")
  end
  
  test "connect to service async" do
    task = Sms506.send_sms_async("81111111","prueba")
    assert {:error, "99"} == Task.await(task)
  end
end
