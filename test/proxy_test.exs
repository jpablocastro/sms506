defmodule Sms506.ProxyTest do
  use ExUnit.Case

  alias Sms506.Proxy

  test "gets api key from config" do
    assert Proxy.api_key != nil
  end

  test "gets base url from config" do
    assert Proxy.base_url != nil
  end

  test "gets timeout from config" do
    assert Proxy.timeout != nil
  end

  test "connect to service" do 
    assert {:error, "99"} == Proxy.send_sms("81111111","prueba")
  end
end
