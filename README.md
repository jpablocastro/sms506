# Sms506

Client for Costa Rican SMS sender provider SMS 506.

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add sms506 to your list of dependencies in `mix.exs`:

        def deps do
          [{:sms506, "~> 0.1.0"}]
        end

  2. Ensure sms506 is started before your application:

        def application do
          [applications: [:sms506]]
        end

