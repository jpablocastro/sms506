defmodule Sms506.Proxy do
  @moduledoc false

  def api_key, do: Application.get_env(:sms506, :api_key)

  def base_url, do: Application.get_env(:sms506, :base_url)

  def timeout, do: Application.get_env(:sms506, :timeout)

  def send_sms(phone, message) do
    with {:ok, response} <- HTTPoison.get("#{base_url}/#{api_key}/t=#{phone}&m=#{message}", headers, options),
         do: parse_response(response.body)
  end

    defp parse_response("0") do
      {:ok, "0"}
    end

    defp parse_response(code) do
      {:error, code}
    end

    defp headers do
      []
    end

    defp options do
      [{:timeout, timeout}, {:recv_timeout, timeout}]
    end
end
