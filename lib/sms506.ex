defmodule Sms506 do
  @moduledoc """
  Client for Costa Rican SMS sender provider SMS 506.

  ## Configuration

      config :sms506, base_url: "http://api.sms506.com/sms/",
                      timeout: 1500,
                      api_key: "your_api_key"
  """

  @doc """
  Sends an SMS `message` to a `phone`.
  """
  def send_sms(phone, message) do 
    Sms506.Proxy.send_sms(phone, message)
  end

  @doc """
  Sends an SMS `message` to a `phone` using an asynchronous Task
  """
  def send_sms_async(phone, message) do 
    Task.async(fn -> Sms506.Proxy.send_sms(phone, message) end)
  end

end
