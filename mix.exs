defmodule Sms506.Mixfile do
  use Mix.Project

  def project do
    [app: :sms506,
     version: "0.2.0",
     elixir: "~> 1.2",
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     description: description,
     package: package,
     deps: deps]
  end

  # Configuration for the OTP application
  #
  # Type "mix help compile.app" for more information
  def application do
    [applications: [:logger, :httpoison]]
  end

  defp description do
    """
    Client for Costa Rican SMS sender provider SMS 506.
    """
  end

  defp package do
    [# These are the default files included in the package
     name: :sms506,
     files: ["lib", "priv", "mix.exs", "README*", "license*"],
     maintainers: ["José Pablo Castro"],
     licenses: ["Apache 2.0"],
     links: %{"Bitbucket" => "https://bitbucket.org/jpablocastro/sms506",
              "Docs" => "https://hexdocs.pm/sms506/"}]
  end

  # Dependencies can be Hex packages:
  #
  #   {:mydep, "~> 0.3.0"}
  #
  # Or git/path repositories:
  #
  #   {:mydep, git: "https://github.com/elixir-lang/mydep.git", tag: "0.1.0"}
  #
  # Type "mix help deps" for more examples and options
  defp deps do
    [
      {:httpoison, "~> 0.7.2"},
      {:earmark, "~> 0.1", only: :dev},
      {:ex_doc, "~> 0.11", only: :dev}
    ]
  end
end
